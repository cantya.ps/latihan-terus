INSERT INTO vets VALUES (1, 'James', 'Carter', 20);
INSERT INTO vets VALUES (2, 'Helen', 'Leary', 25);
INSERT INTO vets VALUES (3, 'Linda', 'Douglas', 32);
INSERT INTO vets VALUES (4, 'Rafael', 'Ortega', 30);
INSERT INTO vets VALUES (5, 'Henry', 'Stevens', 28);
INSERT INTO vets VALUES (6, 'Sharon', 'Jenkins',40);

INSERT INTO specialties VALUES (1, 'Advance Programing');
INSERT INTO specialties VALUES (2, 'Data Structure Algorithm');
INSERT INTO specialties VALUES (3, 'Matdas');

INSERT INTO vet_specialties VALUES (1, 1);
INSERT INTO vet_specialties VALUES (2, 2);
INSERT INTO vet_specialties VALUES (3, 3);
INSERT INTO vet_specialties VALUES (4, 2);
INSERT INTO vet_specialties VALUES (5, 1);
INSERT INTO vet_specialties VALUES (6, 3);

INSERT INTO types VALUES (1, 'cat');
INSERT INTO types VALUES (2, 'dog');
INSERT INTO types VALUES (3, 'lizard');
INSERT INTO types VALUES (4, 'snake');
INSERT INTO types VALUES (5, 'bird');
INSERT INTO types VALUES (6, 'hamster');

INSERT INTO students VALUES (1, 'George', 'Franklin', '110 W. Liberty St.', 'Madison', '6085551023');
INSERT INTO students VALUES (2, 'Betty', 'Davis', '638 Cardinal Ave.', 'Sun Prairie', '6085551749');
INSERT INTO students VALUES (3, 'Eduardo', 'Rodriquez', '2693 Commerce St.', 'McFarland', '6085558763');
INSERT INTO students VALUES (4, 'Harold', 'Davis', '563 Friendly St.', 'Windsor', '6085553198');
INSERT INTO students VALUES (5, 'Peter', 'McTavish', '2387 S. Fair Way', 'Madison', '6085552765');
INSERT INTO students VALUES (6, 'Jean', 'Coleman', '105 N. Lake St.', 'Monona', '6085552654');
INSERT INTO students VALUES (7, 'Jeff', 'Black', '1450 Oak Blvd.', 'Monona', '6085555387');
INSERT INTO students VALUES (8, 'Maria', 'Escobito', '345 Maple St.', 'Madison', '6085557683');
INSERT INTO students VALUES (9, 'David', 'Schroeder', '2749 Blackhawk Trail', 'Madison', '6085559435');
INSERT INTO students VALUES (10, 'Carlos', 'Estaban', '2335 Independence La.', 'Waunakee', '6085555487');

INSERT INTO courses VALUES (1, 'Leo', '2000-09-07', 1, 1, NULL, NULL);
INSERT INTO courses VALUES (2, 'Basil', '2002-08-06', 6, 2, 1, NULL);
INSERT INTO courses VALUES (3, 'Rosy', '2001-04-17', 2, 3, 1, NULL);
INSERT INTO courses VALUES (4, 'Jewel', '2000-03-07', 2, 3, 2, 3);
INSERT INTO courses VALUES (5, 'Iggy', '2000-11-30', 3, 4, NULL, 3);
INSERT INTO courses VALUES (6, 'George', '2000-01-20', 4, 5, 2, 3);
INSERT INTO courses VALUES (7, 'Samantha', '1995-09-04', 1, 6, NULL, 3);
INSERT INTO courses VALUES (8, 'Max', '1995-09-04', 1, 6, 6, 7);
INSERT INTO courses VALUES (9, 'Lucky', '1999-08-06', 5, 7, 6, 7);
INSERT INTO courses VALUES (10, 'Mulligan', '1997-02-24', 2, 8, 6, 7);
INSERT INTO courses VALUES (11, 'Freddy', '2000-03-09', 5, 9, 8, NULL);
INSERT INTO courses VALUES (12, 'Lucky', '2000-06-24', 2, 10, 8, 9);
INSERT INTO courses VALUES (13, 'Sly', '2002-06-08', 1, 10, 8, 9);

INSERT INTO visits VALUES (1, 7, '2013-01-01', 'rabies shot', 1);
INSERT INTO visits VALUES (2, 8, '2013-01-02', 'rabies shot', 2);
INSERT INTO visits VALUES (3, 8, '2013-01-03', 'neutered', 3);
INSERT INTO visits VALUES (4, 7, '2013-01-04', 'spayed', 4);
